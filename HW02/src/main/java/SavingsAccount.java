/**
 * Created by codyb on 9/22/2016.
 */
public class SavingsAccount {
    private static double annualInterestRate;
    private double savingsBalance;

    public static void modifyInterestRate(double newRate) {
        annualInterestRate = newRate;
    }

    public double calculateMonthlyInterest() {
        double monthlyInterest = annualInterestRate * savingsBalance / 12;
        savingsBalance += monthlyInterest;

        return monthlyInterest;
    }

    public double getSavingsBalance() {
        return savingsBalance;
    }

    public void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    public void print() {
        System.out.println(savingsBalance);
    }
}
