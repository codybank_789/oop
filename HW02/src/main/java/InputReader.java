import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class InputReader {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(new File("C:\\Users\\codyb\\Google Drive\\Study\\Me\\GIT\\oop\\test.txt"));
            input.useDelimiter("[.,: \\n]+");

            while(input.hasNext()) {
                System.out.println(input.next());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}