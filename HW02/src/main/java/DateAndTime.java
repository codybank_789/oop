/**
 * Created by codyb on 9/22/2016.
 */
public class DateAndTime {
    private int secondSinceMidnight;
    private Date date;

    public DateAndTime(int h, int m, int s, int D, int M, int Y) {
        setTime(h, m, s);
        date = new Date(M, D, Y);
    }

    public void setTime( int h, int m, int s )
    {
        setHour( h );    
        setMinute( m );  
        setSecond( s );  
    }  

    public void setHour( int h )
    {
        int hour = ( ( h >= 0 && h < 24 ) ? h : 0 );
        secondSinceMidnight = secondSinceMidnight%3600 + hour*3600;
    }  

    public void setMinute( int m )
    {
        int minute = ( ( m >= 0 && m < 60 ) ? m : 0 );
        int hour = secondSinceMidnight/3600;

        secondSinceMidnight = secondSinceMidnight%60 + minute*60 + hour*3600;
    }  

    public void setSecond( int s )
    {
        int second = ( ( s >= 0 && s < 60 ) ? s : 0 );
        secondSinceMidnight = secondSinceMidnight/60*60 + second;
    }  

    public int getHour()
    {
        return secondSinceMidnight/3600;
    }  

    public int getMinute()
    {
        return secondSinceMidnight%3600/60;
    }  

    public int getSecond()
    {
        return secondSinceMidnight%60;
    }  

    public String toUniversalString()
    {
        return String.format(
                "%02d:%02d:%02d %s", getHour(), getMinute(), getSecond(), date.toString());
    }  

     
    public String toString()
    {
        return String.format( "%d:%02d:%02d %s %s",
                ( (getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12 ),
                getMinute(), getSecond(), ( getHour() < 12 ? "AM" : "PM" ) , date.toString());
    }  

    public void incrementSecond() {
        secondSinceMidnight++;
        refresh();
    }

    public void incrementMinute() {
        secondSinceMidnight += 60;
        refresh();
    }

    public void incrementHour() {
        secondSinceMidnight += 3600;
        refresh();
    }

    private void refresh() {
        if(secondSinceMidnight / (60*60*24) == 1) {
            date.nexTDay();
            secondSinceMidnight %= (60*60*24);
        }
    }

    public static void main(String[] args) {
        DateAndTime dateAndTime = new DateAndTime(0, 0, 0, 28, 1, 1997);

        int t = 2000;
        while(t > 0) {
            t--;
            System.out.println(dateAndTime.toString());
            dateAndTime.incrementHour();
        }
    }
}
