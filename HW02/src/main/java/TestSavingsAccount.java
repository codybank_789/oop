/**
 * Created by codyb on 9/22/2016.
 */
public class TestSavingsAccount {
    public static void main(String[] args) {
        SavingsAccount savingsAccount1 = new SavingsAccount();
        SavingsAccount savingsAccount2 = new SavingsAccount();

        savingsAccount1.setSavingsBalance(2000);
        savingsAccount2.setSavingsBalance(3000);

        SavingsAccount.modifyInterestRate(0.04);

        savingsAccount1.calculateMonthlyInterest();
        savingsAccount2.calculateMonthlyInterest();

        savingsAccount1.print();
        savingsAccount2.print();


        SavingsAccount.modifyInterestRate(0.05);

        savingsAccount1.calculateMonthlyInterest();
        savingsAccount2.calculateMonthlyInterest();

        savingsAccount1.print();
        savingsAccount2.print();
    }
}
