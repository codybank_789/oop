/**
 * Created by codyb on 9/22/2016.
 */
public class Employee {
    private String firstName;
    private String lastName;
    private double mothlySalary;

    public Employee(String firstName, String lastName, double mothlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mothlySalary = mothlySalary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getMothlySalary() {
        return mothlySalary;
    }

    public void setMothlySalary(double mothlySalary) {
        this.mothlySalary = mothlySalary;

        if(this.mothlySalary <= 0) {
            this.mothlySalary = 0.0;
        }
    }
}
