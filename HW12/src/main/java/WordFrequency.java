import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by cody on 06/12/2016.
 */
public class WordFrequency {
    public static void main(String[] args) {
        int validSize = 0;

        if(args.length > 0) {
            validSize = Integer.parseInt(args[0]);
        }

        Scanner input = new Scanner(System.in);

        Map<String, Integer> count = new HashMap<String, Integer>();

        String result = null;
        count.put(result, 0);

        while (input.hasNext()) {
            String next = input.next();

            if(next.length() < validSize) continue;

            if(!count.containsKey(next)) {
                count.put(next, 0);
            }

            count.put(next, count.get(next)+1);
            result = (count.get(result) > count.get(next)) ? result : next;
        }

        System.out.println(result + " " + count.get(result));
    }
}
