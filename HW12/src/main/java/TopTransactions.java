import java.util.*;

/**
 * Created by cody on 06/12/2016.
 */
public class TopTransactions {
    static class Transaction implements Comparator<Transaction> {
        String data;
        double transValue;

        public Transaction() {
        }

        public Transaction(String data, double transValue) {
            this.data = data;
            this.transValue = transValue;
        }

        public int compare(Transaction o1, Transaction o2) {
            return (int)-(o1.transValue - o2.transValue);
        }

        @Override
        public String toString() {
            return data + " " + String.valueOf(transValue);
        }
    }

    public static void main(String[] args) {
        int count = 1;
        if(args.length != 0) {
            count = Integer.parseInt(args[0]);
        }

        Scanner input = new Scanner(System.in);
        Queue<Transaction> list = new PriorityQueue<Transaction>(new Transaction());

        while(input.hasNext()) {
            list.add(new Transaction(input.next() + " " + input.next(), Double.parseDouble(input.next())));
        }

        while(count-- > 0 && !list.isEmpty()) {
            System.out.println(list.poll().toString());
        }
    }
}
