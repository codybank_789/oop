public class RandomWalk { 

    public static void main(String[] args) {
        int[][] dir = {
            {1,0},
            {0,-1},
            {-1,0},
            {0,1}
        };
        
        int n = Integer.parseInt(args[0]);
        StdDraw.setXscale(-n, +n);
        StdDraw.setYscale(-n, +n);
        StdDraw.clear(StdDraw.GRAY);
        StdDraw.enableDoubleBuffering();

        int x = 0, y = 0;
        int count = 0;
        int curLength = 1;
        int curDir = 0;
        int steps = 0;
        while (Math.abs(x) < n && Math.abs(y) < n) {
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledSquare(x, y, 0.45);
            
            steps++;
            x += dir[curDir][0];
            y += dir[curDir][1];
            curLength--;
            if (curLength == 0) {
                count++;
                curLength = count/2+1;
                curDir++;
                curDir %= 4;
            }

            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.filledSquare(x, y, 0.45);
            StdDraw.show();
            StdDraw.pause(40);
        }
        StdOut.println("Total steps = " + steps);
    }

}