class ThreeSort {
	public static void main (String[] args) {
		int[] values = new int[3];
		for(int i = 0; i < 3; i++) {
			values[i] = Integer.parseInt(args[i]);
		}

		int max = Math.max(Math.max(values[0], values[1]), values[2]);
		int min = Math.min(Math.min(values[0], values[1]), values[2]);
		int mid = values[0] + values[1] + values[2] - max - min;
		System.out.println(min + " " + mid + " " + max);
	}
}