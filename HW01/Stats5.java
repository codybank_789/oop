class Stats5 {
	public static void main (String[] args) {
		double cur = Math.random();
		double max = cur;
		double min = cur;
		double ave = cur;
		System.out.println(cur);
		for(int i = 1; i < 5; i++) { 
			cur = Math.random();
			max = Math.max(max, cur);
			min = Math.min(min, cur);
			ave += cur;
			System.out.println(cur);
		}

		System.out.println(ave / 5 + " " + min + " " + max);
	}
}