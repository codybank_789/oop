public class MaxMin {
	public static void main (String[] args) {
		int max = Integer.parseInt(args[0]);
		int min = Integer.parseInt(args[0]);

		for(int i = 1; i < args.length; i++) {
			max = Math.max(max, Integer.parseInt(args[i]));
			min = Math.min(min, Integer.parseInt(args[i]));
		}

		System.out.println(max + " " + min);
	}

}