public class AllEqual {
	public static void main (String[] args) {
		int[] num = new int[3];
		for(int i = 0; i < 3; i++) {
			num[i] = Integer.parseInt(args[i]);
		}

		System.out.println((num[0] == num[1] && num[1] == num[2]) ? "equal" : "not equal");
	}
}