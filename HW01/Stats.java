class Stats {
	public static void main (String[] args) {
		int n = Integer.parseInt(args[0]);

		double ave = 0;
		for(int i = 1; i <= n; i++) {
			ave += Double.parseDouble(args[i]);
		}
		ave /= n;
		double result = 0;
		for(int i = 1; i <= n; i++) {
			double current = Double.parseDouble(args[i]);
			result += (current - ave) * (current - ave);
		}

		result = Math.sqrt(result) / (n-1);

		System.out.println(ave + " " + result);
	}
}