import junit.framework.TestCase;

/**
 * Created by codyb on 10/6/2016.
 */
public class StackTest extends TestCase {

    public void testPush() throws Exception {

        Stack stack = new Stack();

        stack.push("Minh");
        stack.push("Ngan");
    }

    public void testPop() throws Exception {
        Stack stack = new Stack();

        stack.push("Minh");
        stack.push("Ngan");

        assertEquals("Ngan", stack.pop());
        assertEquals("Minh", stack.pop());
        assertNull(stack.pop());
    }

    public void testIsEmpty() throws Exception {
        Stack stack = new Stack();

        assertTrue(stack.isEmpty());

        stack.push("aaa");

        assertFalse(stack.isEmpty());

        stack.pop();

        assertTrue(stack.isEmpty());
    }
}