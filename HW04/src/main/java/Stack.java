/**
 * Created by codyb on 10/4/2016.
 */
public class Stack {
    private class Node {
        Node next;
        String data;

        public Node(Node next, String data) {
            this.next = next;
            this.data = data;
        }
    }

    Node head = null;

    public Stack() {}

    public void push(String data) {
        Node newNode = new Node(head, data);
        head = newNode;
    }

    public String pop() {
        if(isEmpty()) return null;

        String result = head.data;
        head = head.next;

        return result;
    }

    public boolean isEmpty() {
        return head == null;
    }
}