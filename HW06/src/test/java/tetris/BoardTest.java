package tetris;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardTest {
	Board b;
	Piece pyr1, pyr2, pyr3, pyr4, s, sRotated;

	// This shows how to build things in setUp() to re-use
	// across tests.

	// In this case, setUp() makes shapes,
	// and also a 3X6 board, with pyr placed at the bottom,
	// ready to be used by tests.
	@Before
	public void setUp() throws Exception {
		b = new Board(10, 20);

		pyr1 = new Piece(Piece.PYRAMID_STR);
		pyr2 = pyr1.computeNextRotation();
		pyr3 = pyr2.computeNextRotation();
		pyr4 = pyr3.computeNextRotation();

		s = new Piece(Piece.S1_STR);
		sRotated = s.computeNextRotation();

		b.place(pyr1, 0, 0);
		b.commit();
	}

	// Check the basic width/height/max after the one placement
	@Test
	public void testSample1() {
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(2, b.getColumnHeight(1));
		assertEquals(2, b.getMaxHeight());
		assertEquals(3, b.getRowWidth(0));
		assertEquals(1, b.getRowWidth(1));
		assertEquals(0, b.getRowWidth(2));
	}

	// Place sRotated into the board, then check some measures
	@Test
	public void testSample2() {
		b.commit();
		int result = b.place(sRotated, 1, 1);
		assertEquals(Board.PLACE_OK, result);
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(4, b.getColumnHeight(1));
		assertEquals(3, b.getColumnHeight(2));
		assertEquals(4, b.getMaxHeight());
	}

	// Make  more tests, by putting together longer series of
	// place, clearRows, undo, place ... checking a few col/row/max
	// numbers that the board looks right after the operations.
	@Test
	public void testDropHeight() {
		b.commit();
		Piece one = new Piece("0 0 0 1 1 0 2 0");
		b.place(one, 0, 2);
		assertEquals(3, b.getRowWidth(0));
		assertEquals(1, b.getRowWidth(1));
		assertEquals(3, b.getRowWidth(2));
		assertEquals(1, b.getRowWidth(3));
		assertEquals(0, b.getRowWidth(4));
		assertEquals(4, b.getColumnHeight(0));
		assertEquals(3, b.getColumnHeight(1));
		assertEquals(3, b.getColumnHeight(2));

		Piece two = new Piece("0 0 1 0 1 1 0 1");
		assertEquals(4, b.dropHeight(two, 0));
		assertEquals(3, b.dropHeight(two, 1));
	}

	@Test
	public void testHeightWidthGrid() {
		Piece one = new Piece("0 0 0 1 1 0 1 1");
		b.place(one, 0, b.dropHeight(one, 0));
		b.commit();
		Piece two = new Piece("0 0 0 1 0 2 0 3");
		two = two.computeNextRotation();
		b.place(two, 2, 1);
		b.commit();
		Piece three = new Piece("0 0 1 0 1 1 2 1");
		b.place(three, 5, 0);
		b.commit();
		Piece four = new Piece("0 0 1 0 1 1 1 2");
		b.place(four, 8, 0);
		b.commit();
		assertEquals(4, b.getColumnHeight(0));
		assertEquals(2, b.getColumnHeight(3));
		assertEquals(1, b.getColumnHeight(8));
		assertEquals(3, b.getColumnHeight(9));
		assertEquals(7, b.getRowWidth(0));
		assertEquals(8, b.getRowWidth(1));
		assertEquals(3, b.getRowWidth(2));
		assertEquals(2, b.getRowWidth(3));
		assertEquals(false, b.getGrid(7, 0));
		assertEquals(true, b.getGrid(5, 0));
	}

	@Test
	public void testClearRows(){
		int result;
		Piece one = new Piece("0 0 0 1 1 1 1 2");
		result = b.place(one, 0, b.dropHeight(one, 0));
		b.commit();
		assertEquals(0, result);

		Piece two = new Piece("0 0 0 1 0 2 0 3");
		two = two.computeNextRotation();
		b.place(two, 2, 1);
		b.commit();

		Piece three = new Piece("0 0 1 0 1 1 2 1");
		b.place(three, 5, 0);
		b.commit();

		Piece four = new Piece("0 0 1 0 1 1 1 2");
		b.place(four, 8, 0);
		b.commit();

		Piece five = new Piece("0 0 0 1 0 2 1 2");
		result = b.place(five, 8, 1);
		b.commit();
		assertEquals(1, result);
		int clearRow = b.clearRows();

	}

	@Test
	public void testClearTwoRows(){
		Piece one = new Piece("0 0 0 1 1 1 1 2");
		int drop = b.dropHeight(one, 0);
		b.place(one, 0, drop);
		b.commit();
		assertEquals(1, drop);

		/*	Piece two = one.computeNextRotation();
			b.place(two, 2, 0);
			b.commit();*/

		Piece three = pyr1.computeNextRotation();
		b.place(three, 4, 0);
		b.commit();

		Piece four = new Piece("0 0 1 0 0 1 1 1");
		b.place(four, 6, 0);
		b.commit();
		b.place(four, 8, 0);
		b.commit();
		b.place(four, 2, 1);
		b.commit();
		assertEquals(1, b.clearRows());

			/*assertEquals("true true true true true true true true true true", b.toStringGrid(0));
			assertEquals(2, b.clearRows());*/
	}

	@Test
	public void testClearRowAndUndo(){
		Piece one = new Piece("0 0 0 1 1 0 1 1");
		b.place(one, 0, 2);
		b.commit();
		assertEquals(4, b.getColumnHeight(0));


		Piece two = new Piece("0 0 0 1 0 2 1 0");
		b.place(two, 3, 0);
		b.commit();

		Piece three = new Piece("0 1 1 1 1 0 2 0");
		b.place(three, 5, 0);
		b.commit();

		Piece four = new Piece("0 0 1 0 2 0 2 1");
		b.place(four, 7, 1);

		assertEquals(7, b.getRowWidth(1));

		b.undo();
		assertEquals(0, b.getColumnHeight(9));
		assertEquals(3, b.getRowWidth(2));

		b.place(four, 7, 1);
		b.commit();

		Piece five = new Piece("0 0 0 1 0 2 1 2");
		b.place(five, 2, 1);
		b.commit();

		Piece six = new Piece("0 0 0 1 1 1 2 1");
		b.place(six, 4, 1);
		b.commit();

		b.place(one, 7, 2);
		b.commit();
		assertEquals(10, b.getRowWidth(2));
		assertEquals(4, b.getColumnHeight(2));
		assertEquals(1, b.clearRows());
		assertEquals(6, b.getRowWidth(2));
		b.undo();
		assertEquals(10, b.getRowWidth(2));
		int result = b.place(one, 6, 3);
		assertEquals(Board.PLACE_BAD, result);
		assertEquals(4, b.getColumnHeight(7));
	}

	@Test
	public void testUndoClearRow(){
		Piece one = new Piece("0 0 0 1 1 0 1 1");
		b.place(one, 3, 0);
		b.commit();

		Piece two = new Piece("1 0 0 0 2 0 3 0");
		b.place(two, 6, 0);
		b.commit();

		Piece three = new Piece("0 0 0 1 1 1 1 2");
		b.place(three, 5, 0);

		assertEquals(1, b.clearRows());
		b.undo();
		assertEquals(0, b.getColumnHeight(5));

		Piece four = new Piece("0 0 0 1 1 0 1 1");
		b.clearRows();
		b.commit();
		b.place(four, 7, 1);
		assertEquals(3, b.getColumnHeight(7));
	}

	@Test
	public void testClearRowHighest(){
		Piece one = new Piece("0 0 1 0 2 0 3 0");
		b.place(one, 0, 2);
		b.commit();

		Piece two = new Piece("0 0 0 1 1 0 1 1");
		b.place(two, 5, 0);
		b.commit();

		Piece three = new Piece("0 2 1 2 1 1 1 0");
		b.place(three, 8, 0);
		b.commit();

		Piece four = new Piece("0 0 0 1 0 2 1 0");
		b.place(four, 4, 2);
		b.commit();

		Piece five = new Piece("0 0 1 0 1 1 1 2");
		b.place(five, 6, 2);

		assertEquals(10, b.getRowWidth(2));
		b.clearRows();
		assertEquals(2, b.getRowWidth(2));
		assertEquals(4, b.getColumnHeight(4));
	}





}
