package tetris;

public class CustomBrain extends DefaultBrain {

    public double rateBoard(Board board) {
        final int width = board.getWidth();
        final int maxHeight = board.getMaxHeight();

        int sumHeight = 0;
        int holes = 0;

        // Count the holes, and sum up the heights
        for (int x=0; x<width; x++) {
            final int colHeight = board.getColumnHeight(x);
            sumHeight += colHeight;

            int y = colHeight - 2;    // addr of first possible hole

            while (y>=0) {
                if  (!board.getGrid(x,y)) {
                    int delta = y;
                    holes += delta * delta;
                }
                y--;
            }

            int neighbourHeight= 0;
            if(x > 0) neighbourHeight = board.getColumnHeight(x-1);
            if(x < width-1) neighbourHeight = Math.max(neighbourHeight, board.getColumnHeight(x+1));

            if(board.getColumnHeight(x) < neighbourHeight) {
                int delta = neighbourHeight - board.getColumnHeight(x);
                holes += delta * delta * delta / 3;
            }
        }

        double avgHeight = ((double)sumHeight)/width;

        // Add up the counts to make an overall score
        // The weights, 8, 40, etc., are just made up numbers that appear to work
        return (10*maxHeight + 5*avgHeight + 20*holes);
    }

}

