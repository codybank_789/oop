// Board.java
package tetris;
/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
 */
public class Board	{
    // Some ivars are stubbed out for you:
    private int width;
    private int height;
    private int[] rowStatus;
    private int[] oldStatus;
    private int[] colHeight;
    private int[] oldHeight;
    private int maxHeight;
    private boolean DEBUG = true;
    boolean committed;


    // Here a few trivial methods are provided:

    /**
     Creates an empty board of the given width and height
     measured in blocks.
     */
    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        committed = true;

        rowStatus = new int[2*height];
        oldStatus = new int[2*height];

        colHeight = new int[width];
        oldHeight = new int[width];

        init();
    }

    private void init() {
        for(int i = 0; i < width; i++) {
            colHeight[i] = -1;
            oldHeight[i] = -1;
        }

        maxHeight = -1;
    }

    /**
     Returns the width of the board in blocks.
     */
    public int getWidth() {
        return width;
    }


    /**
     Returns the height of the board in blocks.
     */
    public int getHeight() {
        return height;
    }


    /**
     Returns the max column height present in the board.
     For an empty board this is 0.
     */
    public int getMaxHeight() {
        return maxHeight+1; // YOUR CODE HERE
    }


    /**
     Checks the board for internal consistency -- used
     for debugging.
     */
    public void sanityCheck() {
        if (DEBUG) {
            // YOUR CODE HERE
        }
    }

    /**
     Given a piece and an x, returns the y
     value where the piece would come to rest
     if it were dropped straight down at that x.

     <p>
     Implementation: use the skirt and the col heights
     to compute this fast -- O(skirt length).
     */
    public int dropHeight(Piece piece, int x) {
        int[] skirt = piece.getSkirt();

        int result = 0;
        for(int i = 0; i < skirt.length; i++) {
            result = Math.max(result, colHeight[x + i] + 1 - skirt[i]);
        }

        return result;
    }


    /**
     Returns the height of the given column --
     i.e. the y value of the highest block + 1.
     The height is 0 if the column contains no blocks.
     */
    public int getColumnHeight(int x) {
        return colHeight[x]+1; // YOUR CODE HERE
    }


    /**
     Returns the number of filled blocks in
     the given row.
     */
    public int getRowWidth(int y) {
        return Integer.bitCount(rowStatus[y]); // YOUR CODE HERE
    }


    /**
     Returns true if the given block is filled in the board.
     Blocks outside of the valid width/height area
     always return true.
     */

    private boolean getBit(int x, int pos) {
        return (x >> pos & 1) == 1;
    }
    public boolean getGrid(int x, int y) {
        return getBit(rowStatus[y], x); // YOUR CODE HERE
    }

    private boolean getGrid(TPoint point) {
        return getGrid(point.x, point.y);
    }


    public static final int PLACE_OK = 0;
    public static final int PLACE_ROW_FILLED = 1;
    public static final int PLACE_OUT_BOUNDS = 2;
    public static final int PLACE_BAD = 3;

    /**
     Attempts to add the body of a piece to the board.
     Copies the piece blocks into the board grid.
     Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
     for a regular placement that causes at least one row to be filled.

     <p>Error cases:
     A placement may fail in two ways. First, if part of the piece may falls out
     of bounds of the board, PLACE_OUT_BOUNDS is returned.
     Or the placement may collide with existing blocks in the grid
     in which case PLACE_BAD is returned.
     In both error cases, the board may be left in an invalid
     state. The client can use undo(), to recover the valid, pre-place state.
     */

    private boolean isInside(TPoint point) {
        return 0 <= point.x && point.x < width && 0 <= point.y && point.y < height;
    }
    private TPoint add(TPoint a, TPoint b) {
        return new TPoint(a.x + b.x, a.y + b.y);
    }
    public int place(Piece piece, int x, int y) {
        // flag !committed problem
        if (!committed) throw new RuntimeException("place commit problem");

        committed = false;
        int result = PLACE_OK;

        // YOUR CODE HERE
        TPoint delta = new TPoint(x, y);
        TPoint[] body = piece.getBody();
        for(TPoint point : body) {
            TPoint a = add(point, delta);

            if(!isInside(a)) {
                return PLACE_OUT_BOUNDS;
            }

            if(getGrid(a)) {
                return PLACE_BAD;
            }

            rowStatus[a.y] += 1 << a.x;
            if(rowStatus[a.y] == (1 << width) - 1) {
                result = PLACE_ROW_FILLED;
            }

            colHeight[a.x] = Math.max(colHeight[a.x], a.y);
            maxHeight = Math.max(maxHeight, a.y);
        }

        return result;
    }


    /**
     Deletes rows that are filled all the way across, moving
     things above down. Returns the number of rows cleared.
     */
    public int clearRows() {
        committed = false;

        int rowsCleared = 0;

        for(int i = 0; i-rowsCleared < height; i++) {
            if(rowStatus[i] == (1 << width) - 1) {
                rowsCleared++;
            } else {
                rowStatus[i - rowsCleared] = rowStatus[i];
            }
        }

        int oldMaxHeight = maxHeight;
        maxHeight = -1;
        for(int i = 0; i < width; i++) {
            colHeight[i] = -1;
            for(int j = oldMaxHeight; j >= 0; j--) {
                if(getBit(rowStatus[j], i)) {
                    colHeight[i] = j;
                    maxHeight = Math.max(maxHeight, colHeight[i]);
                    break;
                }
            }
        }

        return rowsCleared;
    }



    /**
     Reverts the board to its state before up to one place
     and one clearRows();
     If the conditions for undo() are not met, such as
     calling undo() twice in a row, then the second undo() does nothing.
     See the overview docs.8
     */

    private void rollback() {
        for(int i = 0; i < rowStatus.length; i++) {
            rowStatus[i] = oldStatus[i];
        }

        maxHeight = -1;
        for(int i = 0; i < colHeight.length; i++) {
            colHeight[i] = oldHeight[i];
            maxHeight = Math.max(maxHeight, colHeight[i]);
        }
    }
    public void undo() {
        if(committed) return;

        rollback();
        committed = true;
    }


    /**
     Puts the board in the committed state.
     */
    public void commit() {
        if (committed) return;

        committed = true;

        for(int i = 0; i < rowStatus.length; i++) {
            oldStatus[i] = rowStatus[i];
        }

        for(int i = 0; i < colHeight.length; i++) {
            oldHeight[i] = colHeight[i];
        }
    }



    /*
     Renders the board state as a big String, suitable for printing.
     This is the sort of print-obj-state utility that can help see complex
     state change over time.
     (provided debugging utility)
     */
    public String toString() {
        StringBuilder buff = new StringBuilder();
        for (int y = height-1; y>=0; y--) {
            buff.append('|');
            for (int x=0; x<width; x++) {
                if (getGrid(x,y)) buff.append('+');
                else buff.append(' ');
            }
            buff.append("|\n");
        }
        for (int x=0; x<width+2; x++) buff.append('-');
        return(buff.toString());
    }
}


