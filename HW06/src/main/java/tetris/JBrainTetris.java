package tetris;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bloe on 10/25/16.
 */
public class JBrainTetris extends JTetris {

    /**
     * Creates a new JTetris where each tetris square
     * is drawn with the given number of pixels.
     *
     * @param pixels
     */
    PrintWriter out;
    private Brain brain;
    private int lastCount;
    private Brain.Move bestMove;
    protected int totalScore = 0;
    protected int countTime = 0;
    String type = "";

    JBrainTetris(int pixels, PrintWriter out, Brain brain, String type) {
        super(pixels);

        this.type = type;
        this.out = out;
        this.brain = brain;

        lastCount = 0;
        bestMove = new Brain.Move();
    }

    JCheckBox brainMode;

    @Override
    public JComponent createControlPanel() {
        JComponent panel = (JPanel) super.createControlPanel();

        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        brainMode.setSelected(true);
        panel.add(brainMode);

        return panel;
    }



    @Override
    public void tick(int verb) {
        if (brainMode.isSelected()) {

            verb = getVerb(bestMove);
            super.tick(verb);

            if (gameOn && !bestMove.piece.equals(currentPiece)) {
                super.tick(ROTATE);
            }

            if (gameOn && verb != DOWN) {
                super.tick(DOWN);
            }
        } else {
            super.tick(verb);
        }
    }

    @Override
    public Piece pickNextPiece() {
        return super.pickNextPiece();
    }

    @Override
    public void addNewPiece() {
        super.addNewPiece();

        if (brainMode.isSelected()) {
            board.undo();
            bestMove = brain.bestMove(board, currentPiece, board.getHeight(), bestMove);
        }
    }

    @Override
    public void stopGame() {
        super.stopGame();

        totalScore += score;
        if(countTime <= 50) {
            out.println(String.valueOf(countTime) + " : " + score);
            startGame();
        } else {
            out.println("Total : " + totalScore);
            System.out.println(type + " " + totalScore);
            out.close();
        }
    }

    @Override
    public void startGame() {
        super.startGame();
        countTime++;
    }

    private int getVerb(Brain.Move bestMove) {
        if (currentX > bestMove.x) {
            return LEFT;
        } else if (currentX < bestMove.x) {
            return RIGHT;
        }

        return DOWN;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
        }

        PrintWriter def = null;
        PrintWriter cus = null;
        try {
            File defOutput = new File("/home/minh/DefaultBrain.txt");
            defOutput.createNewFile();
            File cusOutput = new File("/home/minh/CustomBrain.txt");
            cusOutput.createNewFile();

            def = new PrintWriter(defOutput);
            cus = new PrintWriter(cusOutput);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        JBrainTetris tetris = new JBrainTetris(16, def, new DefaultBrain(), "def");
        JFrame frame = JBrainTetris.createFrame(tetris);
        frame.setVisible(true);

        tetris.startGame();

        JBrainTetris tetris1 = new JBrainTetris(16, cus, new CustomBrain(), "cus");
        JFrame frame1 = JBrainTetris.createFrame(tetris1);
        frame1.setVisible(true);

        tetris1.startGame();
    }
}
