/**
 * Created by codyb on 10/10/2016.
 */
public abstract class BankAccount {
    protected double mBalance;
    protected int countTransaction = 0;
    protected double currentFee = 0.0;

    public BankAccount(double mBalance) {
        this.mBalance = mBalance;
    }

    public boolean deposit(double amount) {
        mBalance += amount;

        currentFee += depositFee(amount);
        return true;
    }

    protected abstract double depositFee(double amount);

    public boolean withdraw(double amount) {
        if(mBalance >= amount) {
            mBalance -= amount;
            currentFee += withdrawFee(amount);
            return true;
        }

        return false;
    }

    protected abstract double withdrawFee(double amount);

    public abstract double endMonthCharge();

    public void endMonth() {
        System.out.println("Balance: " + mBalance);
        System.out.println("Transaction: " + countTransaction);
        System.out.println("Fee: " + currentFee);

        mBalance -= endMonthCharge();
        countTransaction = 0;
    }
}
