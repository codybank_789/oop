import java.util.Random;

/**
 * Created by codyb on 10/10/2016.
 */
public class Gambler extends BankAccount{
    Random random = new Random();
    public static final double LUCK_CHANCE = 0.49;

    public Gambler(double mBalance) {
        super(mBalance);

        random.setSeed(2108);
    }

    private int isLucky() {
        return (random.nextDouble() <= LUCK_CHANCE) ? 0 : 1;
    }

    @Override
    protected double depositFee(double amount) {
        return amount * isLucky() * 2;
    }

    @Override
    protected double withdrawFee(double amount) {
        return amount * isLucky() * 2;
    }

    @Override
    public double endMonthCharge() {
        return currentFee;
    }
}
