package Stack;

/**
 * Created by bloe on 10/25/16.
 */
public interface StackOfGenerics<T> extends Iterable<T> {
    void push(T a);
    T pop();
}
