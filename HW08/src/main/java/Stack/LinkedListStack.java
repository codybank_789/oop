package Stack;

import java.util.Iterator;

/**
 * Created by bloe on 10/25/16.
 */
public class LinkedListStack<T> implements StackOfGenerics<T> {

    class LinkedListStackIterator implements Iterator<T> {
        Node curPos;

        public LinkedListStackIterator() {
            curPos = top;
        }

        @Override
        public boolean hasNext() {
            return curPos != null;
        }

        @Override
        public T next() {
            T result = curPos.data;
            curPos = curPos.next;

            return result;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListStackIterator();
    }

    class Node {
        T data;
        Node next;

        public Node(T data, Node next) {
            this.data = data;
            this.next = next;
        }
    }

    Node top = null;

    public LinkedListStack() {}

    public void push(T a) {
        Node newNode = new Node(a, top);
        top = newNode;
    }

    public T pop() {
        if(top != null) {
            T result = top.data;
            top = top.next;

            return result;
        }

        return null;
    }
}
