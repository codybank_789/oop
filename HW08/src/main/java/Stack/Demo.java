package Stack;

import java.util.Iterator;

/**
 * Created by bloe on 11/10/16.
 */
public class Demo {

    public static void main(String[] args) {
        StackOfGenerics<Integer> myStack = new ArrayStack<>();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);

        Iterator it = myStack.iterator();

        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
