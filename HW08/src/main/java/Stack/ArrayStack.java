package Stack;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by bloe on 10/25/16.
 */
public class ArrayStack<T> implements StackOfGenerics<T> {

    private ArrayList<T> stack;

    public ArrayStack() {
        stack = new ArrayList<T>();
    }

    public void push(T a) {
        stack.add(a);
    }

    public T pop() {
        return stack.remove(stack.size()-1);
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayStackIterator();
    }

    private class ArrayStackIterator implements Iterator<T> {

        int curPos;
        public ArrayStackIterator() {
            curPos = 0;
        }

        @Override
        public boolean hasNext() {
            return curPos < stack.size();
        }

        @Override
        public T next() {
            return stack.get(curPos++);
        }
    }
}
