package sort;

/**
 * Created by bloe on 10/25/16.
 */
public class MergeSort {

    public static <T> void  sort (MyComparator<T> compare, T list[]) {
        T[] tmp = (T[]) new Object[list.length];
        mergeSort(list, compare, tmp, 0, list.length-1);
    }

    private static <T> void mergeSort(T list[], MyComparator<T> comparator, T tmp[], int left, int right) {
        if(left >= right) {
            return;
        }

        int middle = (left + right) / 2;
        mergeSort(list, comparator, tmp, left, middle);
        mergeSort(list, comparator, tmp, middle+1, right);

        int curLeft = left;
        int curRight = middle+1;

        for(int i = 0; i < right-left+1; i++) {
            if(curLeft <= middle && (curRight > right || comparator.less(list[curLeft], list[curRight]))) {
                tmp[i] = list[curLeft++];
            } else {
                tmp[i] = list[curRight++];
            }
        }

        for(int i = 0; i < right-left+1; i++) {
            list[left+i] = tmp[i];
        }
    }
}
