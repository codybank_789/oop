package sort;

/**
 * Created by bloe on 10/25/16.
 */
public interface MyComparator<T> {
    boolean less(T a1, T a2);
}
