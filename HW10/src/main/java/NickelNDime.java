/**
 * Created by codyb on 10/10/2016.
 */
public class NickelNDime extends BankAccount{
    public static final double FEE = 2000;

    public NickelNDime(double mBalance) {
        super(mBalance);
    }

    @Override
    protected double depositFee(double amount) {
        return FEE;
    }

    @Override
    protected double withdrawFee(double amount) {
        return FEE;
    }

    @Override
    public double endMonthCharge() {
        return currentFee;
    }
}
