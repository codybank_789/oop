import People.Employee;

import java.util.Date;

/**
 * Created by bloe on 10/18/16.
 */
public class PeopleTest  {
    public static void main(String[] args) {
        Employee newbie = new Employee

                ("Newbie", new Date("2/10/1989"), 1000000);

        Manager boss = new Manager

                ("Boss", new Date("2/23/1986"), 4000000);

        boss.setAssistant(newbie);

        Manager biggerBoss = new Manager

                ("Big Boss", new Date("3/12/1969"), 10000000);

        biggerBoss.setAssistant(boss);

        System.out.println(newbie);

        System.out.println(boss);

        System.out.println(biggerBoss);


        Employee workers[] = new Employee[3];
        workers[0] = newbie;
        workers[1] = boss;
        workers[2] = biggerBoss;
        for(int i = 0; i < workers.length; i++) {
            System.out.println(workers[i]);
        }
    }
}