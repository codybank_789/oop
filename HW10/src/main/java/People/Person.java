package People;

import java.util.Date;

/**
 * Created by codyb on 10/9/2016.
 */
public class Person {
    private String name;
    private Date birthday;

    public Person(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Name " + name + "\n" + "Birthday " + birthday.toString() + "\n";
    }
}