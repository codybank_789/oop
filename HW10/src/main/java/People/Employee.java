package People;

import java.util.Date;

/**
 * Created by codyb on 10/9/2016.
 */
public class Employee extends Person {
    private double salary;

    public Employee(String name, Date birthday, double salary) {
        super(name, birthday);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return super.toString() + "Salary " + String.valueOf(this.getSalary()) + "\n";
    }
}