/**
 * Created by codyb on 10/10/2016.
 */
public abstract class BankAccount {

    class InvalidAmountException extends Exception {
        public InvalidAmountException() {
            super("invalid amount");
        }
    }

    class OverdrawException extends Exception {
        public OverdrawException() {
            super("overdrawn");
        }
    }

    protected double mBalance;
    protected int countTransaction = 0;
    protected double currentFee = 0.0;

    public BankAccount(double mBalance) {
        this.mBalance = mBalance;
    }

    public boolean deposit(double amount) throws InvalidAmountException {
        if(amount < 0) {
            throw new InvalidAmountException();
        }

        mBalance += amount;

        currentFee += depositFee(amount);
        return true;
    }

    protected abstract double depositFee(double amount);

    public boolean withdraw(double amount) throws InvalidAmountException, OverdrawException {
        if(amount < 0) {
            throw new InvalidAmountException();
        }

        if(mBalance >= amount) {
            mBalance -= amount;
            currentFee += withdrawFee(amount);
            return true;
        } else {
            throw new OverdrawException();
        }

    }

    protected abstract double withdrawFee(double amount);

    public abstract double endMonthCharge();

    public void endMonth() {
        System.out.println("Balance: " + mBalance);
        System.out.println("Transaction: " + countTransaction);
        System.out.println("Fee: " + currentFee);

        mBalance -= endMonthCharge();
        countTransaction = 0;
    }
}
