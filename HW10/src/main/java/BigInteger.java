import java.util.*;

public class BigInteger implements Comparable<BigInteger>
{
    private ArrayList<Long> data = new ArrayList();
    private int sign = 1;
    private static String INPUT_INVALID = "invalid input";


    private void validateInput(String input) throws Exception {
        if(input.length() == 0) {
            throw new Exception(INPUT_INVALID);
        }

        for(int i = input.length()-1; i > 0; i--) {
            char cur = input.charAt(i);
            if('0' > cur || cur > '9') {
                throw new Exception(INPUT_INVALID);
            }
        }

        char cur = input.charAt(0);
        if(('0' > cur || cur > '9') && (cur != '-')) {
            throw new Exception(INPUT_INVALID);
        }
    }

    public BigInteger(Long init) {

        if(init < 0) {
            sign = -1;
            init = Math.abs(init);
        }

        while(init > 0) {
            data.add(init%10);
            init /= 10;
        }

        refine();
    }

    public BigInteger(String init) throws Exception {
        validateInput(init);

        if(init.charAt(0) == '-') {
            sign = -1;
            init = init.substring(1);
        }
        if(init.charAt(0) == '+') {
            init = init.substring(1);
        }

        for(int i = init.length()-1; i >= 0; i--) {
            data.add(Long.valueOf(init.charAt(i) + ""));
        }

        refine();
    }

    private void refine() {
        while(data.get(data.size()-1) == 0 && data.size() > 1) {
            data.remove(data.size()-1);
        }
    }

    private BigInteger(ArrayList<Long> init, int sign) {
        this.sign = sign;
        data = init;

        refine();
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        if(sign == -1) {
            result.append("-");
        }
        for(int i = data.size()-1; i >= 0; i--) {
            result.append(String.valueOf(data.get(i)));
        }

        return result.toString();
    }

    public boolean equals(Object other) {
        if(other instanceof BigInteger) {
            return data.equals(((BigInteger) other).data);
        }

        return false;
    }

    public long toLong() {
        long result = 0;
        for(int i = data.size()-1; i >= 0; i--) {
            result = result*10 + data.get(i);
        }

        return result;
    }

    private long get(int pos) {
        if(pos >= data.size()) {
            return 0;
        }
        return data.get(pos);
    }

    private int size() {
        return data.size();
    }

    private void setSign(int sign) {
        this.sign = sign;
    }

    private void switchSign() {
        sign = -sign;
    }

    private BigInteger plus(BigInteger other) {
        int size = Math.max(this.size(), other.size());

        long rem = 0;
        ArrayList<Long> result = new ArrayList<Long>();
        for(int i = 0; i < size; i++) {
            result.add(rem + this.get(i) + other.get(i));
            rem = result.get(i) / 10;
            result.set(i, result.get(i) % 10);
        }

        if(rem != 0) {
            result.add(rem);
        }

        return new BigInteger(result, 1);
    }

    private BigInteger minus(BigInteger other) {
        int compare = this.compareTo(other);

        int size = Math.max(this.size(), other.size());

        long rem = 0;
        ArrayList<Long> result = new ArrayList<Long>();
        for(int i = 0; i < size; i++) {
            long tmp = this.get(i) - other.get(i);
            if(compare == -1) {
                tmp = -tmp;
            }
            tmp -= rem;

            if(tmp < 0) {
                rem = 1;
                tmp += 10;
            } else {
                rem = 0;
            }

            result.add(tmp);
        }

        return new BigInteger(result, compare);
    }



    public BigInteger add(BigInteger other) {
        boolean changedSign = false;
        if(this.sign == -1) {
            this.sign = 1;
            other.switchSign();
            changedSign = true;
        }

        BigInteger result;
        if(other.sign == -1) {
            result = this.minus(other);
        } else {
            result = this.plus(other);
        }

        if(changedSign) {
            this.sign = -1;
            other.switchSign();
            result.switchSign();
        }

        if(result.sign == 0) {
            result.sign = 1;
        }
        return result;
    }

    public BigInteger subtract(BigInteger other) {
        other.switchSign();
        BigInteger result = this.add(other);
        other.switchSign();

        return result;
    }

    public int compareTo(BigInteger other) {
        if(this.size() == other.size()) {
            for(int i = this.size()-1; i >= 0; i--) {
                if(this.get(i) != other.get(i)) {
                    return (int)Math.signum(this.get(i) - other.get(i));
                }
            }

            return 0;
        }

        return (int)Math.signum(this.size() - other.size());
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ArrayList<Long> result = new ArrayList<Long>();

        for(int i = 0; i < this.size(); i++) {
            result.add(this.get(i));
        }

        return new BigInteger(result, this.sign);
    }

    public static void main(String[] args) throws Exception {
        BigInteger one = new BigInteger(5L);
        BigInteger two = new BigInteger("5");

        BigInteger result = one.add(two);

        System.out.println(one.toString() + " " + two.toString());
        System.out.println(result.toString() + " " + result.toLong());
        System.out.println(one.compareTo(two));
        System.out.println(result.compareTo(two));
    }
}