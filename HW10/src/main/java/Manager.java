import People.Employee;

import java.util.Date;

/**
 * Created by codyb on 10/9/2016.
 */
public class Manager extends Employee {
    private Employee assistant;

    public Manager(String name, Date birthday, double salary) {
        super(name, birthday, salary);
    }

    public void setAssistant(Employee assistant) {
        this.assistant = assistant;
    }

    @Override
    public String toString() {
        return super.toString() + "People.Employee " + assistant.toString();
    }
}