/**
 * Created by codyb on 10/10/2016.
 */
public class FlatFee extends BankAccount {
    public static final double MONTHLY_CHARGE = 10000;

    public FlatFee(double mBalance) {
        super(mBalance);
    }

    @Override
    protected double depositFee(double amount) {
        return 0.0;
    }

    @Override
    protected double withdrawFee(double amount) {
        return 0.0;
    }

    @Override
    public double endMonthCharge() {
        return MONTHLY_CHARGE;
    }
}
