package Stacks;

/**
 * Created by bloe on 10/25/16.
 */
public interface StackOfString {
    void push(String a);
    String pop();
}
