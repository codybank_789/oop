package Stacks;

import java.util.ArrayList;

/**
 * Created by bloe on 10/25/16.
 */
public class ArrayStack implements StackOfString{

    private ArrayList<String> stack;

    public ArrayStack() {
        stack = new ArrayList<String>();
    }

    public void push(String a) {
        stack.add(a);
    }

    public String pop() {
        return stack.remove(stack.size()-1);
    }
}
