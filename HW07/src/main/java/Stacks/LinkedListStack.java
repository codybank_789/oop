package Stacks;

/**
 * Created by bloe on 10/25/16.
 */
public class LinkedListStack implements StackOfString {
    class Node {
        String data;
        Node next;

        public Node(String data, Node next) {
            this.data = data;
            this.next = next;
        }
    }

    Node top = null;

    public LinkedListStack() {}

    public void push(String a) {
        Node newNode = new Node(a, top);
        top = newNode;
    }

    public String pop() {
        if(top != null) {
            String result = top.data;
            top = top.next;

            return result;
        }

        return null;
    }
}
