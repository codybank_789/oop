import java.util.Random;

/**
 * Created by bloe on 10/25/16.
 */
public class Demo {
    public static void main(String[] args) {
        Random rand = new Random();
        int count = rand.nextInt();
        BankAccount[] list = new BankAccount[count];
        for(int i = 0; i < count; i++) {
            list[i] = new FlatFee(rand.nextInt());
            list[i].countTransaction = rand.nextInt();
        }

        MergeSort.sort(new BalanceAscending(), list);
        MergeSort.sort(new BalanceDescending(), list);
    }
}
