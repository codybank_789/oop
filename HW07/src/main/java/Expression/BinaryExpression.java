package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public abstract class BinaryExpression implements Expression {
    protected Expression leftExpression;
    protected Expression rightExpression;

    public BinaryExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public Expression getLeftExpression() {
        return leftExpression;
    }

    public Expression getRightExpression() {
        return rightExpression;
    }

    protected String toString(String operator) {
        return "(" + leftExpression.toString() + " " + operator + " " + rightExpression.toString() + ")";
    }
}
