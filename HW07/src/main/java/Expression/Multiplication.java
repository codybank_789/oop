package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public class Multiplication extends BinaryExpression {
    public Multiplication(Expression leftExpression, Expression rightExpression) {
        super(leftExpression, rightExpression);
    }

    public String toString() {
        return toString("*");
    }

    public int evaluate() {
        return 0;
    }
}
