package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public class Addition extends BinaryExpression {
    public Addition(Expression firstExpression, Expression secondExpression) {
        super(firstExpression, secondExpression);
    }

    public int evaluate() {
        return leftExpression.evaluate() + rightExpression.evaluate();
    }

    public String toString() {
        return toString("+");
    }
}
