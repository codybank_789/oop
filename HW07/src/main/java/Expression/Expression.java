package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public interface Expression {
    String toString();
    int evaluate();
}
