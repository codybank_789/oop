package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public class Numeral implements Expression {
    private int value;

    public Numeral(int value) {
        this.value = value;
    }

    public int evaluate() {
        return value;
    }

    public String toString() {
        return String.valueOf(value);
    }
}
