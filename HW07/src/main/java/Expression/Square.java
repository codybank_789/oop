package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public class Square implements Expression {
    private Expression expression;

    public Square(Expression expression) {
        this.expression = expression;
    }

    public int evaluate() {
        return (int) Math.pow(expression.evaluate(), 2);
    }

    @Override
    public String toString() {
        return expression.toString() + "^2";
    }
}
