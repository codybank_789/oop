package Expression;

/**
 * Created by bloe on 10/25/16.
 */
public class Subtraction extends BinaryExpression {
    public Subtraction(Expression leftExpression, Expression rightExpression) {
        super(leftExpression, rightExpression);
    }

    public String toString() {
        return toString("-");
    }

    public int evaluate() {
        return leftExpression.evaluate() - rightExpression.evaluate();
    }
}
