/**
 * Created by bloe on 10/25/16.
 */
public interface MyComparator {
    boolean less(BankAccount a1, BankAccount a2);
}
