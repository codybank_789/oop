/**
 * Created by bloe on 10/25/16.
 */
public class TransactionCountDescending implements MyComparator{
    public boolean less(BankAccount a1, BankAccount a2) {
        return a1.countTransaction > a2.countTransaction;
    }
}
