/**
 * Created by bloe on 10/25/16.
 */
public class BalanceAscending implements MyComparator{
    public boolean less(BankAccount a1, BankAccount a2) {
        return a1.mBalance < a2.mBalance;
    }
}
