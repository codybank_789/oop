/**
 * Created by bloe on 10/25/16.
 */
public class MergeSort {
    static BankAccount tmp[];

    public static void sort(MyComparator compare, BankAccount list[]) {
        tmp = new BankAccount[list.length];
        mergeSort(list, compare, tmp, 0, list.length-1);
    }

    private static void mergeSort(BankAccount list[], MyComparator comparator, BankAccount tmp[], int left, int right) {
        if(left >= right) {
            return;
        }

        int middle = (left + right) / 2;
        mergeSort(list, comparator, tmp, left, middle);
        mergeSort(list, comparator, tmp, middle+1, right);

        int curLeft = left;
        int curRight = middle+1;

        for(int i = 0; i < right-left+1; i++) {
            if(curLeft <= middle && (curRight > right || comparator.less(list[curLeft], list[curRight]))) {
                tmp[i] = list[curLeft++];
            } else {
                tmp[i] = list[curRight++];
            }
        }

        for(int i = 0; i < right-left+1; i++) {
            list[left+i] = tmp[i];
        }
    }
}
