/**
 * Created by cody on 06/12/2016.
 */
public class ForecastDisplay implements Observer, DisplayElement{
    WeatherData weatherData;

    public ForecastDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void update() {
        display();
    }

    public void display() {
        System.out.println("Sorry no update for you =))");
    }
}
