import java.util.ArrayList;
import java.util.List;

/**
 * Created by cody on 06/12/2016.
 */
public class WeatherData implements Subject{
    private List<Observer> observers = new ArrayList<Observer>();
    private double tem, hum;

    public WeatherData(double tem, double hum) {
        this.tem = tem;
        this.hum = hum;
    }

    public WeatherData() {
    }

    public double getTem() {
        return tem;
    }

    public double getHum() {
        return hum;
    }

    public void setTem(double tem) {
        this.tem = tem;

        //notifyObservers();
    }

    public void setHum(double hum) {
        this.hum = hum;

        //notifyObservers();
    }

    public void setMeasurements(double tem, double hum) {
        setTem(tem);
        setHum(hum);

        notifyObservers();
    }

    public void registerObserver(Observer obs) {
        observers.add(obs);
    }

    public void removeObserver(Observer obs) {
        observers.remove(obs);
    }

    public void notifyObservers() {
        observers.forEach(e -> e.update());
    }
}
