/**
 * Created by cody on 06/12/2016.
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement{
    private WeatherData weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void update() {
        display();
    }

    public void display() {
        System.out.println("Current temperature " + String.valueOf(weatherData.getTem()) + " and " + String.valueOf(weatherData.getHum()) + "% humidity");
    }
}
