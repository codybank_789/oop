/**
 * Created by cody on 06/12/2016.
 */
public interface Observer {
    void update();
}
