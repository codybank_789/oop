import java.util.ArrayList;
import java.util.List;

/**
 * Created by cody on 06/12/2016.
 */
public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.setMeasurements(80, 65);
        weatherData.setMeasurements(82, 70);
        weatherData.setMeasurements(78, 90);
    }
}
