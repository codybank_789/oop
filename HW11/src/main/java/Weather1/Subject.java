/**
 * Created by cody on 06/12/2016.
 */
public interface Subject {
    void registerObserver(Observer obs);
    void removeObserver(Observer obs);
    void notifyObservers();
}
