/**
 * Created by cody on 06/12/2016.
 */
public interface DisplayElement {
    void display();
}
