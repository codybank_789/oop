package Weather2;

/**
 * Created by cody on 09/12/2016.
 */
public interface DisplayElement {
    void display();
}
