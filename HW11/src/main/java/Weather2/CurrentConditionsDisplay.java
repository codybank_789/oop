package Weather2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by cody on 06/12/2016.
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
    private WeatherData weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }


    public void display() {
        System.out.println("Current temperature " + String.valueOf(weatherData.getTemp()) + " and " + String.valueOf(weatherData.getHum()) + "% humidity");
    }

    public void update(Observable o, Object arg) {
        display();
    }
}
