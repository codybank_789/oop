package Weather2;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by cody on 06/12/2016.
 */
public class StatisticsDisplay implements Observer, DisplayElement {
    WeatherData weatherData;
    List<Double> tempRec = new ArrayList<Double>();

    public StatisticsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }

    public void display() {
        final double res[] = new double[3];
        res[0] = Double.MIN_VALUE;
        res[1] = Double.MAX_VALUE;
        res[2] = 0f;

        tempRec.forEach(e -> {
            res[0] = Math.max(res[0], e);
            res[1] = Math.min(res[1], e);
            res[2] += e;
        });

        System.out.println(res[0] + " " + res[1] + " " + (res[2] / tempRec.size()));
    }

    public void update(Observable o, Object arg) {
        tempRec.add(weatherData.getTemp());

        display();
    }
}
