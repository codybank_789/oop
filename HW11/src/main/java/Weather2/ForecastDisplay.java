package Weather2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by cody on 06/12/2016.
 */
public class ForecastDisplay implements Observer, DisplayElement {
    WeatherData weatherData;

    public ForecastDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.addObserver(this);
    }


    public void display() {
        System.out.println("Sorry no update for you =))");
    }

    public void update(Observable o, Object arg) {
        display();
    }
}
