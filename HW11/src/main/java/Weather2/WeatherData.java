package Weather2;

import java.util.Observable;

/**
 * Created by cody on 09/12/2016.
 */
public class WeatherData extends Observable{
    private double temp;
    private double hum;

    public WeatherData() {
    }

    public void setMeasurements(double temp, double hum) {
        this.temp = temp;
        this.hum = hum;

        measurementsChanged();
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    public double getTemp() {
        return temp;
    }

    public double getHum() {
        return hum;
    }
}
