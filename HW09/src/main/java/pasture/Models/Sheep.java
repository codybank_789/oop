package pasture.Models;

import com.sun.org.apache.xpath.internal.SourceTree;
import pasture.Entity;
import pasture.Pasture;

import javax.print.attribute.standard.MediaSize;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minh on 11/29/16.
 */
public class Sheep extends Animals{
    private static final String NAME = "sheep";
    private static final ImageIcon IMAGE_ICON = Helper.readModelsImage(NAME);

    public Sheep(Properties properties, Pasture pasture) {
        super(properties, pasture);
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public ImageIcon getImage() {
        return IMAGE_ICON;
    }

    @Override
    public boolean isCompatible(Entity otherEntity) {
        return !(otherEntity instanceof Fence || otherEntity instanceof Sheep);
    }

    @Override
    public int getFoodChainPosition() {
        return 1;
    }

    private Point curDirection = null;
    @Override
    protected Point getMove() {
        Point c = pasture.getEntityPosition(this);

        java.util.List<Entity> inSight = look().collect(Collectors.toList());
        Optional<Entity> nearbyWolves = inSight.stream()
                .filter(e -> e instanceof Wolves)
                .min((a,b) -> Helper.closerPoint(c, pasture.getPosition(a), pasture.getPosition(b)));
        Optional<Entity> nearbyGrass = inSight.stream()
                .filter(e -> e instanceof Grass)
                .min((a,b) -> Helper.closerPoint(c, pasture.getPosition(a), pasture.getPosition(b)));

        if(nearbyWolves.isPresent()) {
            System.out.println("see wolf");
            return getMove(pasture.getEntityPosition(nearbyWolves.get()), false);
        } else if(nearbyGrass.isPresent()) {
            System.out.println("see grass");
            return getMove(pasture.getEntityPosition(nearbyGrass.get()), true);
        } else {
            if(curDirection == null || c.equals(curDirection)) {
                curDirection = pasture.getRandomPosition();
            }

            System.out.println("move randomly");
            return getMove(curDirection, true);
        }
    }

    @Override
    protected boolean isFood(Entity other) {
        return getFoodChainPosition() > other.getFoodChainPosition();
    }

    @Override
    protected Creature reproduce() {
        return new Sheep(properties, pasture);
    }
}
