package pasture.Models;

import java.util.HashMap;

/**
 * Created by minh on 11/29/16.
 */
public class Accounting {
    HashMap<String, HashMap<String, Integer>> data = new HashMap<>();

    public Accounting() {
    }

    public void change(String type, String state, int change) {
        if(!data.containsKey(type)) {
            data.put(type, new HashMap<>());
        }

        HashMap<String, Integer> dataForType = data.get(type);

        if(!dataForType.containsKey(state)) {
            dataForType.put(state, 0);
        }

        dataForType.put(state, dataForType.get(state) + change);
    }

    public void increase(String type, String state) {
        change(type, state, 1);
    }

    public void decrease(String type, String state) {
        change(type, state, -1);
    }

    public void printInfo() {
        data.forEach((k, v) -> {
            System.out.println(k + " : ");
            v.forEach((a, b) -> {
                System.out.println("    " + a + "  : " + b);
            });
        });
    }

    public String getInfo() {
        StringBuilder result = new StringBuilder();
        data.forEach((k, v) -> {
            //System.out.println(k + " : ");
            result.append(k + " : \n" );
            v.forEach((a, b) -> {
                result.append("    " + a + "  : " + b + "\n");
            });
        });

        return result.toString();
    }
}
