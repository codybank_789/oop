package pasture.Models;

import pasture.Entity;
import pasture.Pasture;

import javax.print.attribute.standard.MediaSize;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minh on 11/29/16.
 */
public class Wolves extends Animals {
    private static final String NAME = "wolf";
    private static final ImageIcon IMAGE_ICON = Helper.readModelsImage(NAME);

    public Wolves(Properties properties, Pasture pasture) {
        super(properties, pasture);
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public ImageIcon getImage() {
        return IMAGE_ICON;
    }

    @Override
    public boolean isCompatible(Entity otherEntity) {
        return !(otherEntity instanceof Wolves || otherEntity instanceof Fence);
    }

    @Override
    public int getFoodChainPosition() {
        return 2;
    }

    Point curDirection = null;
    @Override
    protected Point getMove() {
        Point c = pasture.getEntityPosition(this);

        java.util.List<Entity> inSight = look().collect(Collectors.toList());
        Optional<Entity> nearbySheep = inSight.stream()
                .filter(e -> e instanceof Sheep)
                .min((a,b) -> Helper.closerPoint(c, pasture.getPosition(a), pasture.getPosition(b)));

        if(nearbySheep.isPresent()) {
            return getMove(pasture.getPosition(nearbySheep.get()), true);
        } else {
            if(curDirection == null || c.equals(curDirection)){
                curDirection = pasture.getRandomPosition();
            }

            return getMove(curDirection, true);
        }
    }

    @Override
    protected boolean isFood(Entity other) {
        return getFoodChainPosition() > other.getFoodChainPosition();
    }

    @Override
    protected Creature reproduce() {
        return new Wolves(properties, pasture);
    }
}
