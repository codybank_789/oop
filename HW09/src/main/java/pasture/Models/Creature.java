package pasture.Models;

import pasture.Entity;
import pasture.Pasture;

import java.awt.*;
import java.util.*;

/**
 * Created by minh on 11/29/16.
 */
public abstract class Creature implements Entity{
    private static final String NAME = "Creature";

    protected static final String REPRODUCE_TIME = "reproductionCycle";
    protected static final String REPRODUCE_AGE = "reproductionAge";

    protected static final String STATE_BORN = "born";
    protected static final String STATE_LIVING = "living";
    protected static final String STATE_DIED = "died";

    public static final Accounting accounting = new Accounting();

    protected boolean alive = true;
    protected Properties properties;
    protected Pasture pasture;

    protected int age = 0;

    public Creature(Properties properties, Pasture pasture) {
        accounting.increase(getName(), STATE_BORN);
        accounting.increase(getName(), STATE_LIVING);

        this.properties = properties;
        this.pasture = pasture;
    }

    protected abstract String getName();

    protected int readInt(String propertyName) {
        String result = readString(propertyName);
        return (result == null) ? -1 : Integer.parseInt(result);
    }

    protected String readString(String propertyName) {
        return properties.getProperty(getName() + "." + propertyName);
    }

    protected double readDouble(String propertyName) {
        String result = readString(propertyName);
        return (result == null) ? -1. : Double.parseDouble(result);
    }

    protected void die() {
        accounting.decrease(getName(), STATE_LIVING);
        accounting.increase(getName(), STATE_DIED);

        alive = false;
        pasture.removeEntity(this);
    }

    protected abstract Creature reproduce();
    protected void aging() {
        age++;
        if(age > readInt(REPRODUCE_AGE) && (age - readInt(REPRODUCE_AGE)) % readInt(REPRODUCE_TIME) == 0) {
            Point randomPos = Helper.getRandomMember(pasture.getFreeNeighbours(this));
            if(randomPos == null) return;
            pasture.addEntity(reproduce(), randomPos);
        }
    }

    @Override
    public void tick() {
        //accounting.printInfo();
        aging();
    }
}
