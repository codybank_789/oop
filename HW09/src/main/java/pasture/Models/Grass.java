package pasture.Models;

import pasture.Entity;
import pasture.Pasture;

import javax.swing.*;
import java.util.Properties;

/**
 * Created by minh on 11/29/16.
 */
public class Grass extends Plants {
    private static final String NAME = "grass";
    private static final ImageIcon IMAGE_ICON = Helper.readModelsImage("plant");

    public Grass(Properties properties, Pasture pasture) {
        super(properties, pasture);
    }

    @Override
    protected String getName() {
        return NAME;
    }

    @Override
    public ImageIcon getImage() {
        return IMAGE_ICON;
    }

    @Override
    public boolean isCompatible(Entity otherEntity) {
        return !(otherEntity instanceof Fence || otherEntity instanceof Grass);
    }

    @Override
    public int getFoodChainPosition() {
        return 0;
    }

    @Override
    protected Creature reproduce() {
        return new Grass(properties, pasture);
    }
}
