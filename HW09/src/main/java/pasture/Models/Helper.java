package pasture.Models;

import pasture.PastureGUI;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Created by minh on 11/29/16.
 */
public class Helper {

    public static int closerPoint(Point a, Point b, Point c) {
        double distB = a.distance(b);
        double distC = a.distance(c);

        return (distB == distC) ? 0 : (distB > distC) ? 1 : -1;
    }

    public static ImageIcon readModelsImage(String name) {
        System.out.println("/" + name + ".gif");
        return new ImageIcon(PastureGUI.class.getResource("/" + name + ".gif"));
    }

    public static <T> T getRandomMember(Collection<T> input) {
        Optional<T> result = input.stream().findAny();
        return (result.isPresent()) ? result.get() : null;
    }
}