package pasture.Models;

import pasture.Entity;
import pasture.PastureGUI;

import javax.swing.*;

/**
 * Created by minh on 11/29/16.
 */
public class Fence extends FixedObject{
    private static final String NAME = "fence";
    private static final ImageIcon IMAGE_ICON = Helper.readModelsImage(NAME);

    @Override
    public void tick() {
    }

    @Override
    public ImageIcon getImage() {
        return IMAGE_ICON;
    }

    @Override
    public boolean isCompatible(Entity otherEntity) {
        return false;
    }

    @Override
    public int getFoodChainPosition() {
        return 10000000;
    }
}
