package pasture.Models;

import pasture.Entity;
import pasture.Pasture;

import java.awt.*;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by minh on 11/29/16.
 */
public abstract class Animals extends Creature {
    protected static final String MOVE_TIME = "movementCycle";
    protected static final String TIME_WITHOUT_FOOD = "timeWithoutFood";
    protected static final String SIGHT_RANGE = "sightRange";

    protected int countTimeWithoutFood = 0;

    public Animals(Properties properties, Pasture pasture) {
        super(properties, pasture);
    }

    protected Point getMove(Point des, boolean moveTo) {
        int d = (moveTo) ? 1 : -1;

        Collection<Point> possibleMove = pasture.getFreeNeighbours(this);
        possibleMove.add(pasture.getEntityPosition(this));

        Optional<Point> result =  possibleMove.stream()
                .min((a, b) -> Helper.closerPoint(des, a, b) * d);

        return (result.isPresent()) ? result.get() : null;
    }
    protected abstract Point getMove();
    protected void move() {
        if (age % readInt(MOVE_TIME) == 0 && age != 0) {
            if (pasture.getFreeNeighbours(this).size() == 0) return;

            pasture.moveEntity(this, getMove());
        }
    }

    protected abstract boolean isFood(Entity other);
    protected void eat() {
        countTimeWithoutFood++;

        pasture.getEntitiesAt(pasture.getPosition(this)).stream()
                .filter(e -> (isFood(e)))
                .forEach(e -> {
                    ((Creature) e).die();
                    countTimeWithoutFood = 0;
                });

        if (countTimeWithoutFood == readInt(TIME_WITHOUT_FOOD)) {
            die();
        }
    }

    protected Stream<Entity> look() {
        return pasture.getEntities().stream()
                .filter(e -> (pasture.getPosition(e).distance(pasture.getPosition(this)) <= readDouble(SIGHT_RANGE)));
    }

    @Override
    public void tick() {
        move();
        eat();
        super.tick();
    }
}
