package pasture;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;

/**
 * The simulation is run by an internal timer that sends out a 'tick'
 * with a given interval. One tick from the timer means that each
 * entity in the pasture should obtain a tick. When an entity obtains
 * a tick, this entity is allowed to carry out their tasks according
 * to what kind they are. This could mean moving the entity, making
 * the entity starve from hunger, or producing a new offspring.
 */

public class Engine implements ActionListener {
    
    private final int  SPEED_REFERENCE = 1000; /* 1000 */
    private static int speed   = 10;
    private Timer      timer   = new Timer(SPEED_REFERENCE/speed,this);
    private int        time    = 0;

    private Pasture pasture;

    private List<ActionListener> listeners = new ArrayList<>();


    Engine (Pasture pasture) {
        this.pasture = pasture;
        this.speed = speed;
    }

    public void actionPerformed(ActionEvent event) {
        List<Entity> queue = pasture.getEntities();
        for (Entity e : queue) {
            e.tick();
        }
        pasture.refresh();
        time++;

        listeners.forEach(e -> e.actionPerformed(event));
    }

    public void addListener(ActionListener actionListener) {
        listeners.add(actionListener);
    }

    public void removeListener(ActionListener actionListener) {
        listeners.remove(actionListener);
    }

    public void setSpeed(int speed) {
        timer.setDelay(SPEED_REFERENCE/speed);
    }

    public void start() {
        setSpeed(speed);
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    public int getTime () {
        return time;
    }

}
